package oop.tetris;

import java.util.*;

public class Piece {

    private TPoint[] body;
    private int[] skirt;
    private int width;
    private int height;
    private Piece next;
    static private Piece[] pieces;


    Piece(TPoint[] points) {
        body = points;
        int xmax, ymax;
        xmax = body[0].x;
        ymax = body[0].y;
        for (int i = 0; i < body.length; i++) {
            if (Math.max(body[i].x , xmax) != xmax) {
                xmax = body[i].x;
            }
            if (Math.max(body[i].y , ymax) != ymax) {
                ymax = body[i].y;
            }
        }
        width = xmax + 1;
        height = ymax + 1;

        skirt = new int[width];
        for (int i = 0; i < width; i++) {
            int ymin = 1000;
            for (int j = 0; j < 4; j++) {

                if (body[j].x == i) {
                    if (body[j].y < ymin) {
                        ymin = body[j].y;
                    }
                }

            }
            skirt[i] = ymin;
        }
    }

    
    public Piece(String points) {
        this(parsePoints(points));
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public TPoint[] getBody() {
        return body;
    }

    public int[] getSkirt() {
        return skirt;
    }

    public Piece computeNextRotation() {
        TPoint[] xoay = new TPoint[4];
        for (int i = 0; i < 4; i++) {
            xoay[i] = new TPoint(height - body[i].y - 1, body[i].x);

        }
        return new Piece(xoay);
    }

    public Piece fastRotation() {
        return next;
    }

    public boolean equals(Object obj) {
        // standard equals() technique 1
        if (obj == this) {
            return true;
        }


        if (!(obj instanceof Piece)) {
            return false;
        }
        Piece other = (Piece) obj;

        TPoint[] shape1 = this.body;
        TPoint[] shape2 = other.body;   
        
         for (int k = 0; k < 4 - 1; k++) {
            for (int i = 0; i < 4 - 1; i++) {
                if (shape1[i].x > shape1[i + 1].x || (shape1[i].x == shape1[i + 1].x && shape1[i].y > shape1[i + 1].y)) {
                    TPoint tmp1 = shape1[i];
                    shape1[i] = shape1[i + 1];
                    shape1[i + 1] = tmp1;
                }
            }
        }
         
          for (int k = 0; k < 4 - 1; k++) {
            for (int i = 0; i < 4 - 1; i++) {
                if (shape2[i].x > shape2[i + 1].x || (shape2[i].x == shape2[i + 1].x && shape2[i].y > shape2[i + 1].y)) {
                    TPoint tmp2 = shape2[i];
                    shape2[i] = shape2[i + 1];
                    shape2[i + 1] = tmp2;
                }
            }
        }
        
        
        
        
        for(int i=0; i<body.length; i++){
            if(shape1[i].x!=shape2[i].x || shape1[i].y!=shape2[i].y) return false;
        }
        return true;
    }
    
    public static final String STICK_STR = "0 0	0 1 0 2  0 3";
    public static final String L1_STR = "0 0 0 1 0 2 1 0";
    public static final String L2_STR = "0 0 1 0 1 1	1 2";
    public static final String S1_STR = "0 0	1 0	 1 1  2 1";
    public static final String S2_STR = "0 1	1 1  1 0  2 0";
    public static final String SQUARE_STR = "0 0  0 1  1 0  1 1";
    public static final String PYRAMID_STR = "0 0  1 0  1 1  2 0";
    public static final int STICK = 0;
    public static final int L1 = 1;
    public static final int L2 = 2;
    public static final int S1 = 3;
    public static final int S2 = 4;
    public static final int SQUARE = 5;
    public static final int PYRAMID = 6;

    public static Piece[] getPieces() {

        if (Piece.pieces == null) {

            Piece.pieces = new Piece[]{
                makeFastRotations(new Piece(STICK_STR)),
                makeFastRotations(new Piece(L1_STR)),
                makeFastRotations(new Piece(L2_STR)),
                makeFastRotations(new Piece(S1_STR)),
                makeFastRotations(new Piece(S2_STR)),
                makeFastRotations(new Piece(SQUARE_STR)),
                makeFastRotations(new Piece(PYRAMID_STR)),};
        }


        return Piece.pieces;

    }

    private static Piece makeFastRotations(Piece root) {
        Piece ass = root;
        while (true) {
            Piece temp = ass.computeNextRotation();
            if (temp.equals(root) == true) {
                ass.next = root;
                break;
            }
            ass.next = temp;
            ass = temp;
        }

        return root;
    }

    private static TPoint[] parsePoints(String string) {
        List<TPoint> points = new ArrayList<TPoint>();
        StringTokenizer tok = new StringTokenizer(string);
        try {
            while (tok.hasMoreTokens()) {
                int x = Integer.parseInt(tok.nextToken());
                int y = Integer.parseInt(tok.nextToken());

                points.add(new TPoint(x, y));
            }
        } catch (NumberFormatException e) {
            throw new RuntimeException("Could not parse x,y string:" + string);
        }


        TPoint[] array = points.toArray(new TPoint[0]);
        return array;
    }

    public static void main(String[] args) {
        Piece a = new Piece("1 2  3 1  1 3  2 1");
        Piece b = new Piece("3 1  1 2  1 3  2 1");
        System.out.println(a.equals(b));
    }
}
